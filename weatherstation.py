import Adafruit_DHT
import argparse
import csv
import datetime
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
CSV_DAY = "/".join([dir_path, "measurements.csv"])
CSV_MEAN = "/".join([dir_path, "mean_measurements.csv"])

def main():
    parser = argparse.ArgumentParser(description=
                                     'Execute various weatherstation functions')
    parser.add_argument('--measure',
                        help = 'Measure the humidity and temperature',
                        action = 'store_true')
    parser.add_argument('--draw', help='Draw the latest readings to a chart',
                        action = 'store_true')
    parser.add_argument('--mean',
                        help='Calculate the mean value of the previous day',
                        action = 'store_true')
    parser.add_argument('--draw-mean', help='Draw the mean values to a chart',
                        action = 'store_true')
    args = parser.parse_args()
    w = weatherstation()

    if args.measure:
        try:
            w.read()
        except ValueError:
            pass
    if args.draw:
        w.draw()
    if args.mean:
        w.mean()
    if args.draw_mean:
        w.draw_mean()


class weatherstation():
    def __init__(self):
        self.sensor = Adafruit_DHT.DHT11
        self.pin = 23
        self.hum = None
        self.temp = None
        self.time = None

    def read(self):
        """
        Measures the humidity and temperature, appends it to the csv.
        """
        hum, temp = Adafruit_DHT.read_retry(self.sensor, self.pin)
        if hum is None or temp is None:
            raise ValueError("Can't find correct value")

        # Open the csv file, and write to the correct location.
        # Format: time, hum, temp
        with open(CSV_DAY, 'a') as csvfile:
            w = csv.writer(csvfile, delimiter = ' ', quotechar = '"')
            time = datetime.datetime.now().time()
            w.writerow([time, hum, temp])
            print("Wrote to file.")

    def load_csv(self):
        self.hum = []
        self.temp = []
        self.time = []
        with open(CSV_DAY, 'r') as csvfile:
            r = csv.reader(csvfile, delimiter = ' ', quotechar = '"')
            for row in r:
                self.time.append(row[0])
                self.hum.append(float(row[1]))
                self.temp.append(float(row[2]))

    def draw(self):
        """
        Draws a chart of the past measurements
        """
        import matplotlib.pyplot as plt
        if self.hum is None or self.temp is None or self.time is None:
            self.load_csv()
        plt.plot(self.time, self.temp, color='red', label='Temperature')
        plt.plot(self.time, self.hum, color='blue', label='Humidity')
        plt.xticks(rotation='vertical')
        plt.tight_layout()
        plt.savefig('/var/www/html/today.png')

    def mean(self):
        """
        Calculates the mean of the past values, and resets the CSV.
        """
        if self.hum is None or self.temp is None or self.time is None:
            self.load_csv()
        hum_mean = sum(self.hum)/len(self.hum)
        temp_mean = sum(self.temp)/len(self.temp)
        date = datetime.datetime.now().strftime('%Y-%m-%d')

        # Write away the new value.
        with open(CSV_MEAN, 'a') as csvfile:
            w = csv.writer(csvfile, delimiter = ' ', quotechar = '"')
            w.writerow([date, hum_mean, temp_mean])

        # Remove the CSV_DAY file.
        os.remove(CSV_DAY)

    def draw_mean(self):
        """
        Draws a chart of the past mean measurements.
        """
        import matplotlib.pyplot as plt

        hum = []
        temp = []
        date = []
        with open(CSV_MEAN, 'r') as csvfile:
            r = csv.reader(csvfile, delimiter = ' ', quotechar = '"')
            for row in r:
                date.append(row[0])
                hum.append(float(row[1]))
                temp.append(float(row[2]))

        plt.plot(date, temp, color='red', label='Temperature')
        plt.plot(date, hum, color='blue', label='Humidity')
        plt.xticks(rotation='vertical')
        plt.tight_layout()
        plt.savefig('/var/www/html/mean.png')

if __name__ == "__main__":
    main()
