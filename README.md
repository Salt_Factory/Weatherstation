# Weatherstation

A small Python 3 script to measure humidity and temperature, using a DHT11.
It uses the [Adafruit Python](https://github.com/adafruit/Adafruit_Python_DHT) library for DHT.

The script operates by executing function based on flags:

* measure: measures the current humidity and temperature, and saves them to a CSV file.
* draw: draws a graph of the values, and writes it to /var/www/html, to be shown on an Apache webserver.
* mean: calculates the mean humidity and temperature of the past day, and stores them in a different CSV file.
* draw\_mean: draws a graph of the mean values, and writes it to /var/www/html.

A crontab setup is used to periodically trigger the correct functions.

In my setup, measuring and drawing is done every 15 minutes, and the calculation and drawing of mean values is done every day at 1 AM. The crontab is as follows:

```
*/15 * * * * python3 /path/to/weatherstation.py --measure --draw
0 1 * * * python3 /path/to/weatherstation.py --mean --draw-mean
```
